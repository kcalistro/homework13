import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class WxController implements Initializable{
   
   /*
    * declare variables for each controller
    */
   
   @FXML
   private TextField txtZipcode;
   
   @FXML
   private Button btnGo;  
   
   @FXML
   private Label lblLocation;
   
   @FXML
   private Label lblTime;
   
   @FXML
   private Label lblWeather;
   
   @FXML
   private Label lblTemp;
   
   @FXML
   private Label lblWind;
   
   @FXML
   private Label lblPressure;
   
   @FXML
   private Label lblVisibility;
   
   @FXML
   private ImageView iconWx;
   
   @FXML
   private void handleButtonAction(ActionEvent e) {
     // access model by creating object of WxModel class
     WxModel weather = new WxModel();

     // Check that go btn has been pressed
     if (e.getSource() == btnGo)
     {
       String zipcode = txtZipcode.getText();
       // if the zipcode is valid get weather data from WxModel method and set each controller
       if (weather.getWx(zipcode)) 
       {
         lblLocation.setText(weather.getLocation());
         lblTime.setText(weather.getTime());
         lblWeather.setText(weather.getWeather());
         lblTemp.setText(String.valueOf(weather.getTemp()));
         lblWind.setText(weather.getWind());
         lblPressure.setText(String.valueOf(weather.getPressure()));
         lblVisibility.setText(String.valueOf(weather.getVisibility()));
         iconWx.setImage(weather.getImage());
       }
       // if zipcode is not valid display error message, error image and set all labels to empty
       else   
       {
         lblLocation.setText("Invalid Zipcode");
         lblTime.setText("");
         lblWeather.setText("");
         lblTemp.setText("");
         lblWind.setText("");
         lblPressure.setText("");
         lblVisibility.setText("");
         iconWx.setImage(new Image("./error.png"));
       }
     }
   }
   
   @Override
      public void initialize(URL url, ResourceBundle rb) {
      // TODO
   }
   
}